package com.salon.aussie.controller;

import com.salon.aussie.domain.BarberDetails;
import com.salon.aussie.service.BarberService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/barber")
public class BarberController {

    @Autowired
    BarberService barberService;

    @PostMapping("/register")
    public ResponseEntity createBarberRecord(@RequestBody BarberDetails barberDetails) {
        try {
            return new ResponseEntity<>(barberService.registerBarberDetails(barberDetails), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/details")
    public ResponseEntity getBarberAllRecords() {
        log.info("URL got hit from Angular");
        try {
            log.info(barberService.getBarberAllRecords().toString());
            return new ResponseEntity<>(barberService.getBarberAllRecords(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/details/{id}")
    public ResponseEntity getBarberRecord(@PathVariable long id) {
        try {
            return new ResponseEntity<>(barberService.getBarberRecord(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
