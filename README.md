# SalonBookingApplication
A spring-boot application for booking a reservation in a Salon that checks the availability of barbers, and if they are available, a reservation can be done.

## Software Requirements

* [Java](https://www.oracle.com/java/)
* [Maven](https://maven.apache.org/)
* [Spring Boot](https://spring.io/projects/spring-boot)
* [MongoDB](https://www.mongodb.com/)
* [jUnit](https://junit.org/)
* [mockito](https://site.mockito.org/)
* [Docker](https://www.docker.com/)

## Application Architecture

![](images/Architecture_BarberShop.jpg)

## Application UI

### Home page

![](images/barbershop_home.png)

### Barber Details page

![](images/barbershop_members.png)

### Calendar page

![](images/barbershop_calendar.png)


  
## How to run the application

## Running the application using docker

### Installation

Use the maven command line interface [Maven](https://maven.apache.org/) to install the dependencies

```bash
mvn install -DSkipTests
```

### Test
Use the maven command line interface [Maven](https://maven.apache.org/) to perform unit test
```bash
mvn test
```
### package build 
```bash
mvn package
```
### Docker build
```bash
docker build --rm -f "Dockerfile" -t barbershop-app:latest "."
```


### How the docker image is build
* Base image for the docker container is `adoptopenjdk:11-jre-openj9`

* lib files from the location  `/target/dependency/lib ` are copied to `/app/lib` in the image

* Resources files from the location  `/target/dependency/resources ` are copied to `/app` in the image

* Compiled class files from the location  `/target/classes ` are copied to `/app` in the image

### How to run docker container

```bash
docker container run  -p 3030:3030 -d --rm --name barbershop-app  barbershop-app:latest
```

## How to run the application in Kubernetics

* push the created image to Docker container registry using push command
 
* connect to cloud kubernetes cluster
* Deploy the image to kubernetics pod using apply command
 ` kubectl apply -f deployment.yaml`
* Check the status of the pod
 ` Kubectl get pods`
* Test the application

## How to run the application in Azure Cloud

* Create build pipe line using mvn template or default template
* Define stages for using mvn template, add stages like test,pakage,build image,push image to docker container registry
* Create release piple line in cloud and deploy the deployment.yaml 
 ` kubectl apply -f deployment.yaml`
* Check the status of the pod
 ` Kubectl get pods`
* Test the application

## Running the application with IDE

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `com.salon.aussie.SalonBookingApplication` class from your IDE.

* 	Download the zip or clone the Git repository.
* 	Unzip the zip file (if you downloaded one)
* 	Open Command Prompt and Change directory (cd) to folder containing pom.xml
* 	Open Eclipse
	* File -> Import -> Existing Maven Project -> Navigate to the folder where you unzipped the zip
	* Select the project
* 	Open Intleji
	* File->Open -> Navigate to the folder where you unzipped the zip
	* Select the project
* 	Choose the Spring Boot Application file (search for @SpringBootApplication)
* 	Right Click on the file and Run as Java Application

## Intialize the Data

* Request -http://localhost:3030/barber/register

body1 

  
       "name": "Samson",
        "address": "Almere",
        "phoneNumber": "1234",
        "experience": "5",
        "rating": "8",
        "chargePerSession": "15 euro",
        "barberImg": "barber2.png"

body2 

  
       "name": "Andrea",
        "address": "Almere",
        "phoneNumber": "1234",
        "experience": "7",
        "rating": "10",
        "chargePerSession": "30 euro",
        "barberImg": "barber3.png"

body3 

  
       "name": "Josh",
        "address": "Almere",
        "phoneNumber": "1234",
        "experience": "8",
        "rating": "10",
        "chargePerSession": "35 euro",
        "barberImg": "barber1.png"

## Health Check

+ 
    - path : /application/health
    - port : 3030
    
## Data Contract

+ 
    - path : openapi.yaml
    - open  [swaggerEditor](https://editor.swagger.io) then paste the contents in opeapi.yaml
    
## API Collection  

 - path : https://www.getpostman.com/collections/dadd7c29729a54dc58b0
## Swagger url

  *  [SWAGGER](http://localhost:3030/swagger-ui/index.html)
  







